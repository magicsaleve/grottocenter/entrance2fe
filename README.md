## Rationale

Purpose of this repository is to easily create PDF usable underground from Grottocenter riggings.

## Installation

The simplest way is to create a sandboxed Python virtual environment to install
the prerequisites.

```bash
$ python3 -m venv .env
$ .env/bin/activate
$ pip install -r requirements.txt
```

## Usage

### build a PDF from [Gaël Aven](https://grottocenter.org/ui/entrances/65334)

1. Create a Mardown document (the default Jinja template)

   ```bash
   ./entrance2fe.py 65334 > Aven_Gaël.md
   ```

   Another Jinja template can be specified with option `--template`.

2. Convert the document to PDF

   Using [pandoc](https://pandoc.org/) utility.

   ```bash
   pandoc Aven_Gaël.md -o Aven_Gaël.pdf
   ```
   This will create the following PDF: [Aven_Gaël.pdf](./examples/Aven_Gaël.pdf)