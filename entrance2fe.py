#! /usr/bin/env python3

import argparse
import json
import sys
import urllib.request

import jinja2


def entrance2fe(entrance_id: int, jinja_template: str):
    """
    Write a Markdown document containing the riggings of a Grottercenter entrance

    Args:
      entrance_id: Grottocenter identifier of an entrance i.e the last integer
      in the entrance url. For instance the URL of Aven Gaël entrance is
      https://grottocenter.org/ui/entrances/65334 Its identifier is 65334

      jinja_template: a jinja template file able to manipulate the JSON
      description of the entrance.
    """
    # fetch from Grottercenter a JSON description of the specified entrance
    req = urllib.request.Request(
        f"https://api.grottocenter.org/api/v1/entrances/{entrance_id}",
        None,
        headers=dict(accept="application/json"),
    )
    with urllib.request.urlopen(req) as response:
        entrance = json.load(response)

    # Generate file based on the given Jinja template
    environment = jinja2.Environment()
    with open(jinja_template) as istr:
        template = istr.read()
    template = environment.from_string(template)
    return template.stream(entrance=entrance)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("entrance", type=int)
    parser.add_argument("--template", default="template.jinja")
    args = parser.parse_args()

    md_file = entrance2fe(args.entrance, args.template)
    md_file.dump(sys.stdout)


if __name__ == "__main__":
    main()
